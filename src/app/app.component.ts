import { LocalNotifications } from '@ionic-native/local-notifications';
import { NativeAudio } from '@ionic-native/native-audio';
import { Component } from "@angular/core";
import { Platform, App, Events } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { HomePage } from "../pages/home/home";
import { LocalstorageProvider } from "../providers/localstorage/localstorage";
import { OneSignal } from "@ionic-native/onesignal";
import { onesignalAppId, sendor_id } from "./config";
import { RestfulapiProvider } from "../providers/restfulapi/restfulapi";
import { BackgroundMode } from '@ionic-native/background-mode';
import { PushserviceProvider } from '../providers/pushservice/pushservice';
import { Vibration } from '@ionic-native/vibration';
@Component({
  templateUrl: "app.html"
})
export class MyApp {

  rootPage: any;

  usertoken : any
  // rootPage = "SigninPage";

  is_open : boolean

  // check background and foreground status
  app_status : boolean

  constructor(
    private platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public localstoreage: LocalstorageProvider,
    public onesignal : OneSignal,
    public restfulapi : RestfulapiProvider,
    public nativeaudio : NativeAudio,
    public backgroundmode : BackgroundMode,
    public localnoti : LocalNotifications,
    public pushservice : PushserviceProvider,
    public vibration : Vibration,
    public app: App,
    public event : Events
    

  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();
      statusBar.hide();
      splashScreen.hide();
      this.usertoken = ""
      this.is_open = false
      this.app_status = true

      this.triggerLocalNotificatin()

      this.setsoundpreload()
      this.backgroundmode.enable()

      // app is foreground
      this.platform.pause.subscribe(()=>{
        this.app_status = false
      }, er=>{

      })


      this.platform.resume.subscribe(()=>{
        this.app_status = true
      })
      // this.setsoundpreload()
      this.getLoginStatus()
    });
  }

  playnotificationsound()
  {
      let notiinfo = this.localstoreage.getNotificationInfo()
      this.localstoreage.saveNotiReceive(true)

      if(notiinfo)
      {
        if(notiinfo['vibration'])
        {
          this.vibration.vibrate(1000)
        }
        if(notiinfo['tone'])
        {
          this.playsound(notiinfo['tone'])
        }else{
          this.onesignal.enableSound(true)
        }
      }
  }

  triggerLocalNotificatin()
  {
    
    this.localnoti.on('trigger').subscribe(res=>{

      let userid = this.localstoreage.getuserID()
      let notificationsetting = this.localstoreage.getNotificationInfo()
      console.log("this is local notificatin", notificationsetting);
        this.getuserToken(userid).then(res=>{
          console.log("this is success",res);

          this.playnotificationsound()
          if(this.app_status == true)
          {
            this.event.publish('noti_receive')

          }else{
            this.pushservice.sendPushNotificationRequest(notificationsetting['tone'],notificationsetting['vibration'], res).then(res=>{
            })
          }
          
        }).catch(er=>{
          console.log("this is error", er);

        })

      }, err=>{
        console.log("this is error");

      })


      this.localnoti.on('click').subscribe(res=>{
        let userid = this.localstoreage.getuserID()
        let notificationsetting = this.localstoreage.getNotificationInfo()
        console.log("this is local notificatin", notificationsetting);
          this.getuserToken(userid).then(res=>{
            console.log("this is success",res);
  
            // let unitid = ""
            // if(this.platform.is('android'))
            // {
            //   unitid = notificationsetting['tone'] + "_android"
            // }else if(this.platform.is('ios')){
            //   unitid = notificationsetting['tone'] + "_ios"
            // }
            this.playnotificationsound()
            this.event.publish('noti_receive')
            // this.pushservice.sendPushNotificationRequest(notificationsetting['tone'],notificationsetting['vibration'], res).then(res=>{
                
            // })
          }).catch(er=>{
            console.log("this is error", er);
  
          })
      }, err=>{
        console.log("this is click error");

      })


      let res_array = this.localstoreage.getNotificationInfo()
      console.log(res_array);
  }

  public async getuserToken(userid)
  {

    return new Promise((resolve, reject)=>{
      this.onesignal.startInit(onesignalAppId, sendor_id);
      this.onesignal.inFocusDisplaying(this.onesignal.OSInFocusDisplayOption.InAppAlert);
      this.onesignal.getIds().then(result=>{
        console.log("this is token info", result);
        this.restfulapi.saveUserToken(userid, result['userId']).then(async res=>{
            resolve( result['userId'])
        }).catch(er=>{
          reject(er)
        })

      }).catch(er=>{
        reject(er)
      })
      this.onesignal.endInit()
    })
  }

  setsoundpreload()
  {
    this.nativeaudio.preloadComplex( "tone1_android",'assets/sound/tone1.mp3',1,1,0)
    this.nativeaudio.preloadComplex( "tone1_ios",'assets/sound/tone1.m4r',1,1,0)
    this.nativeaudio.preloadComplex( "tone2_android",'assets/sound/tone2.mp3',1,1,0)
    this.nativeaudio.preloadComplex( "tone2_ios",'assets/sound/tone2.m4r',1,1,0)
    this.nativeaudio.preloadComplex( "tone3_android",'assets/sound/tone3.mp3',1,1,0)
    this.nativeaudio.preloadComplex( "tone3_ios",'assets/sound/tone3.m4r',1,1,0)
  }

  playsound(soundname)
  {
    let unitid = ""
    if(this.platform.is('android'))
    {
      unitid = soundname + "_android"
    }else if(this.platform.is('ios')){
      unitid = soundname + "_ios"
    }else{

    }
    console.log(unitid)
    this.nativeaudio.play(unitid,()=>{
    })
  }

  setOnesignal()
  {
    this.onesignal.startInit(onesignalAppId, sendor_id);
    this.onesignal.inFocusDisplaying(this.onesignal.OSInFocusDisplayOption.None);
    this.onesignal.enableSound(false)
    
    this.onesignal.handleNotificationReceived().subscribe((res)=>{
      // this.onesignal.enableSound(false)
      // let notiinfo = this.localstoreage.getNotificationInfo()
      // this.localstoreage.saveNotiReceive(true)

      // if(notiinfo)
      // {
      //   if(notiinfo['vibration'])
      //   {
      //     this.vibration.vibrate(1000)
      //   }
      //   if(notiinfo['tone'])
      //   {
      //     this.playsound(notiinfo['tone'])
      //   }else{
      //     this.onesignal.enableSound(true)
      //   }
      // }

      // let loginstatus = this.localstoreage.getLoginStatus()
      //   if(loginstatus == 'true')
      //   {
      //     // this.app.getRootNav().getActiveChildNav().select(0);
      //     // this.rootPage= "CustomtabPage"
        
      //     this.event.publish('noti_receive')
      //     // 
      //   }else{
      //     this.rootPage = "SigninPage"
      //   }
    }, er=>{

    })

    this.onesignal.endInit()
    this.getLoginStatus()

  }




  getLoginStatus() {
    let firststatus = this.localstoreage.getFirstStatus()
    let loginstatus = this.localstoreage.getLoginStatus()
    console.log( "great" + firststatus + loginstatus);

    if(firststatus)
    {
      if(loginstatus == 'true')
      {
        this.rootPage= "CustomtabPage"
      }else{
        this.rootPage = "SigninPage"
        // this.rootPage = "WalkthroughPage"
      }
    }else{
      this.rootPage = "WalkthroughPage"
    }

  }

}
