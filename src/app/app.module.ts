import { FilePath } from '@ionic-native/file-path';
import { LocalstorageProvider } from './../providers/localstorage/localstorage';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ValidationProvider } from '../providers/validation/validation';
import { ToastseriveProvider } from '../providers/toastserive/toastserive';
import { Camera} from '@ionic-native/camera';
import { ActionSheet} from '@ionic-native/action-sheet';
import {HttpClientModule } from '@angular/common/http';
import { EmailComposer } from '@ionic-native/email-composer';
import { RestfulapiProvider } from '../providers/restfulapi/restfulapi';
import { HttpModule } from "@angular/http";
import { LoadingProvider } from '../providers/loading/loading';
import { CustomalertProvider } from '../providers/customalert/customalert';
import { UploadphotoProvider } from '../providers/uploadphoto/uploadphoto';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import { OneSignal } from '@ionic-native/onesignal';
import {
  FileTransfer,
  FileTransferObject,
} from "@ionic-native/file-transfer";

import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

import { NativeAudio } from '@ionic-native/native-audio';
import { AppRate } from '@ionic-native/app-rate';
import { LongPressModule } from 'ionic-long-press';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackgroundMode } from '@ionic-native/background-mode';
import { PushserviceProvider } from '../providers/pushservice/pushservice';
import { Vibration } from '@ionic-native/vibration';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      tabsPlacement: "bottom",
      tabsHideOnSubPages: false
    }),
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ValidationProvider,
    ToastseriveProvider,
    Camera,
    FileTransfer,
    FileTransferObject,
    File,
    FilePath,
    ActionSheet,
    LocalstorageProvider,
    EmailComposer,
    RestfulapiProvider,
    ValidationProvider,
    LoadingProvider,
    CustomalertProvider,
    UploadphotoProvider,
    CustomalertProvider,
    SocialSharing,
    OneSignal,
    Facebook,
    GooglePlus,
    NativeAudio,
    LongPressModule,
    AppRate,
    LocalNotifications,
    BackgroundMode,
    PushserviceProvider,
    Vibration
  ]
})
export class AppModule {}
