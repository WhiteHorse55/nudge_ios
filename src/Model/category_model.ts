export class Category_Model{
    ca_id : string
    ca_name : string
    ca_icon : string
    ca_content : string
    updated : string
    u_id : string
    approve : string
    isChecked : boolean

    constructor()
    {
        this.ca_id = ""
        this.ca_name = ""
        this.ca_icon = ""
        this.ca_content = ""
        this.updated = ""
        this.u_id = ""
        this.approve = ""
        this.isChecked = false
    }
}