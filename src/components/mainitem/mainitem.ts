import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the MainitemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mainitem',
  templateUrl: 'mainitem.html'
})
export class MainitemComponent {

  @Input() category : any;
  @Input() icon_name : any;
  @Input() isEdit : any;
  @Input() index : any

  @Output() clickEdit : EventEmitter<any> = new EventEmitter()
  @Output() clickItem : EventEmitter<any> = new EventEmitter()
  @Output() longpressItem : EventEmitter<any> = new EventEmitter()
  constructor() {
    console.log('Hello MainitemComponent Component');
  }

  onclickEditbutton()
  {
    this.clickEdit.emit(this.index)
  }

  onclickItem()
  {
    this.clickItem.emit(this.index)
  }

  clicklongpress()
  {
    this.longpressItem.emit(this.index)
  }

}
