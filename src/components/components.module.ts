import { NgModule } from '@angular/core';
import { CustombarComponent } from './custombar/custombar';
import { IonicModule } from 'ionic-angular';
import { MainbarComponent } from './mainbar/mainbar';
import { MainitemComponent } from './mainitem/mainitem';
import { CategoryitemComponent } from './categoryitem/categoryitem';
import { FavoriteitemComponent } from './favoriteitem/favoriteitem';
@NgModule({
	declarations: [CustombarComponent,
    MainbarComponent,
    MainitemComponent,
    CategoryitemComponent,
    FavoriteitemComponent],
	imports: [IonicModule],
	exports: [CustombarComponent,
    MainbarComponent,
    MainitemComponent,
    CategoryitemComponent,
    FavoriteitemComponent]
})
export class ComponentsModule {}
