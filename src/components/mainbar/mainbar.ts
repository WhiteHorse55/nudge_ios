import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the MainbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mainbar',
  templateUrl: 'mainbar.html'
})
export class MainbarComponent {

  @Input() title : any
  @Input() type : any
  @Output() clickedit : EventEmitter<any> = new EventEmitter()

  constructor() {
    console.log('Hello MainbarComponent Component');

  }

  onclickeditbutton()
  {
    this.clickedit.emit('edit')
  }


}
