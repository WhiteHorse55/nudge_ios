import { UrlService } from './../../Constant/UrlService';
import { ActionSheetController } from "ionic-angular";
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { SocialSharing } from "@ionic-native/social-sharing";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";

/**
 * Generated class for the CategoryitemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "categoryitem",
  templateUrl: "categoryitem.html"
})
export class CategoryitemComponent {
  @Input() category: any;
  @Input() index: any;

  @Output() clickEdit: EventEmitter<any> = new EventEmitter();
  @Output() clickItem: EventEmitter<any> = new EventEmitter();
  @Output() clicklike: EventEmitter<any> = new EventEmitter();
  @Output() clickshare: EventEmitter<any> = new EventEmitter();

  image_url : string
  constructor(
    public actionSheetCtrl: ActionSheetController,
    public socialshare: SocialSharing,
    public toastctrl : ToastseriveProvider
  ) {
    this.image_url = UrlService.SERVER_IMAGE_URL
    console.log("Hello CategoryitemComponent Component");
  }

  onclickEditbutton() {
    this.clickEdit.emit(this.index);
  }

  onclickitem() {
    this.clickItem.emit(this.index);
  }

  onclicklikebutton() {
    this.clicklike.emit(this.index);
  }

  onclicksharebutton() {
    // this.clickshare.emit(this.index)
    this.presentActionSheet();
  }

  ////////////////////////////show take photo actionsheet///////////////////////////////////////////////
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select You can change world by sharing",
      buttons: [
        {
          text: "Facebook",
          handler: () => {
            this.sharewithfacebook()
          }
        },

        {
          text: "Instgram",
          handler: () => {
            this.sharewithinstgram()
          }
        },

        {
          text: "Twitter",
          handler: () => {
            this.sharewithtwitter()
          }
        },

        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });

    actionSheet.present();
  }

  public sharewithtwitter() {

    let sharingobjec : Object = {
      message : this.category.message
    }

    this.socialshare
      .shareViaTwitter(this.category.message)
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install Twitter app first!");
      });
  }

  public sharewithinstgram() {

    this.socialshare
      .shareViaInstagram(this.category.message, "")
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install instagram app first!");
      });
  }

  public sharewithfacebook() {
    // this.socialshare
    //   .shareViaFacebook(this.category.message)
    //   .then(res => {})
    //   .catch(er => {

    //   });

      this.socialshare.canShareVia('facebook', this.category.message).then(res=>{

      }).catch(er=>{
        this.toastctrl.create("Please install facebook app first!");
      })

  }
}
