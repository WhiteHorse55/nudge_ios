import { Component, Input, Output, EventEmitter } from '@angular/core';
import { EventListener } from '@angular/core/src/debug/debug_node';

/**
 * Generated class for the CustombarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custombar',
  templateUrl: 'custombar.html'
})
export class CustombarComponent {

  @Input() title : any
  @Input() subtitle : any
  @Input() type : any
  @Input() rightType : any

  @Output() clickback : EventEmitter<any> = new EventEmitter()
  constructor() {

  }

  onclickbackbutton()
  {
    this.clickback.emit('back')
  }

}
