import { UrlService } from "./../../Constant/UrlService";
import { ActionSheetController } from "ionic-angular";
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { SocialSharing } from "@ionic-native/social-sharing";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";

/**
 * Generated class for the FavoriteitemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "favoriteitem",
  templateUrl: "favoriteitem.html"
})
export class FavoriteitemComponent {
  @Input() category: any;
  @Input() index: any;

  @Output() clickEdit: EventEmitter<any> = new EventEmitter();
  @Output() clickItem: EventEmitter<any> = new EventEmitter();
  @Output() clickshare: EventEmitter<any> = new EventEmitter();

  imageurl: string;
  constructor(
    public actionSheetCtrl: ActionSheetController,
    public socialshare: SocialSharing,
    public toastctrl: ToastseriveProvider
  ) {
    this.imageurl = UrlService.SERVER_IMAGE_URL;
  }

  onclickEditbutton() {
    this.clickEdit.emit(this.index);
  }

  onclickitem() {
    this.clickItem.emit(this.index);
  }

  onclicksharebutton() {
    // this.clickshare.emit(this.index)
    this.presentActionSheet();
  }

  ////////////////////////////show take photo actionsheet///////////////////////////////////////////////
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select You can change world by sharing",
      buttons: [
        {
          text: "Facebook",
          handler: () => {
            this.sharewithfacebook();
          }
        },

        {
          text: "Instgram",
          handler: () => {
            this.sharewithinstgram();
          }
        },

        {
          text: "Twitter",
          handler: () => {
            this.sharewithtwitter();
          }
        },

        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });

    actionSheet.present();
  }

  public sharewithtwitter() {
    this.socialshare
      .shareViaTwitter(this.category.message)
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install Twitter app first!");
      });
  }

  public sharewithinstgram() {
    this.socialshare
      .shareViaInstagram(this.category.message, "")
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install instagram app first!");
      });
  }

  public sharewithfacebook() {
    this.socialshare
      .shareViaFacebook(this.category.message)
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install facebook app first!");
      });

  }
}
