export class ResponseMessage{
  public static INVALID_STRINGLENGTH = "Choose a password at least 6 characters long."
  public static INVALID_CONTAINLETTER = "Choose a password contains a letter."
  public static INVALID_CONTAINNUMBER = "Choose a password contains a number."
  public static INVALID_REENTER = "Please enter correct confirm password"

  public static EMPTY_EMAIL = "Email info is empty"
  public static EMPTY_PASSWORD = "Password info is empty"
  public static EMAIL_EXIST = "Email already exists"

  public static EMPTY_CATEGORY = "Categoryname  is empty"


  public static RESPONSE_SUCCESS = "success"
  public static RESPONSE_FAIL = "fail"

  public static TERMSERVICE="Nudge: Nudge is designed to break your habitually thinking the same thing day in and day out. \n The human mind creates over 60,000 thoughts a day and 98% of them are the same exact thoughts you had yesterday and the day before. \n The end result is little more than noise, which keeps you from achieving your life goals. \n You are stuck in a routine and don’t even know it. \n" +
  "Nudge is designed to get your brain to change the habits it’s stuck in, and get you thinking about new, positive things that will help you create the life you have always dreamed of." +
  "Messages will pop up on your screen. Read them, and either ‘feel’ what is says, or just go do ti. If you can’t do it right at that moment, imagine yourself doing it. Simple!!"
}
