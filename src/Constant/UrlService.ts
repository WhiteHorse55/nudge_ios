export class UrlService{
  // public static SERVER_URL = "http://192.168.1.4/nudge/Api/"
  // public static SERVER_IMAGE_URL = "http://192.168.1.4/nudge/upload/photo/"
  public static SERVER_URL = "http://countrysport-co-uk.stackstaging.com/nudge/Api/"
  public static SERVER_IMAGE_URL = "http://countrysport-co-uk.stackstaging.com/nudge/upload/photo/"
  public static USER_SIGNIN = UrlService.SERVER_URL + "login"
  public static USER_SIGNUP = UrlService.SERVER_URL + "signup"
  public static CHECK_EMAILEXIST = UrlService.SERVER_URL + "checkEmailExists"
  public static SAVE_TOKEN = UrlService.SERVER_URL + "updateToken"

  public static UPLOAD_PHOTO = UrlService.SERVER_URL + 'uploadphoto'

  // category
  public static GET_CATEGORY = UrlService.SERVER_URL + "getcategory"
  public static ADD_CATEGORY = UrlService.SERVER_URL + "addcategory"
  public static ADD_USERCATEGORY = UrlService.SERVER_URL + "addusercategory"
  public static DELETE_CATEGORY = UrlService.SERVER_URL + "deletecategory"
  public static DELETE_USERCATEGORY = UrlService.SERVER_URL + "deleteusercategory"
  public static EDIT_CATEGORY = UrlService.SERVER_URL + "editcategory"
  public static GET_USERCATEGORY = UrlService.SERVER_URL + "getusercategory"

  // message
  public static GET_MESSAGES = UrlService.SERVER_URL + "getmessages"
  public static ADD_MESSAGES = UrlService.SERVER_URL + "addmessages"
  public static EDIT_MESSAGES = UrlService.SERVER_URL + "editmessages"
  public static DELETE_MESSAGES = UrlService.SERVER_URL + "deletemessages"

  // favorite
  public static ADD_FAVORITE = UrlService.SERVER_URL + "saveLike"
  public static GET_FAVORITE = UrlService.SERVER_URL + "getLike"
  public static DELETE_FAVORITE = UrlService.SERVER_URL + "deleteLike"
  public static CHECK_FAVORITE_STATUS = UrlService.SERVER_URL + "checkfavorite"


  // Notification
  public static GET_NOTIFICATION = UrlService.SERVER_URL + "getnotification"
  public static SEEN_NOTIFICATION = UrlService.SERVER_URL + "seenotification"
  public static DELETE_NOTIFICATION = UrlService.SERVER_URL + "deletenotification"

  // forgot password
  public static FORGOT_PASSWORD = UrlService.SERVER_URL + "resetPassword"
  public static UPDATE_PROFILE = UrlService.SERVER_URL + "updateprofile"

  // send pushnotification
  public static SEND_NOTIFICATION = UrlService.SERVER_URL + "sendpushnotificationwithmobile"

  // get new message
  public static GET_NEW_MESSAGE = UrlService.SERVER_URL + "getNewMessages"

}
