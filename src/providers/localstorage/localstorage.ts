import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LocalstorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalstorageProvider {

  constructor(public http: HttpClient) {
    console.log('Hello LocalstorageProvider Provider');
  }


  public saveNotificationInfo(info)
  {
    window.localStorage.setItem('notiinfo', JSON.stringify(info));
  }

  public getNotificationInfo()
  {
    let profile = window.localStorage.getItem('notiinfo');
    return JSON.parse(profile);
  }

  public saveUserInfo(info) {
    window.localStorage.setItem('userinfo', JSON.stringify(info));
  }

  public getUserinfo(): any {
    let profile = window.localStorage.getItem('userinfo');
    return JSON.parse(profile);
  }

  public getuserID()
  {
    return this.getUserinfo()['id'];
  }

  public getusertoken()
  {
    return this.getUserinfo()['token'];
  }


  // public saveCategoryList(cateInfo)
  // {
  //   window.localStorage.setItem('cateinfo', JSON.stringify(cateInfo));
  // }

  // public getCategoryList()
  // {
  //   return JSON.parse(window.localStorage.getItem('cateinfo'))
  // }


  public saveLoginStatus(isLogin) {
    return window.localStorage.setItem('isLogin', isLogin);
  }

  public getLoginStatus() {
    return window.localStorage.getItem('isLogin');
  }

  public saveFirstStatus(isFirst) {
    return window.localStorage.setItem('isFirst', isFirst);
  }

  public getFirstStatus() {
    return window.localStorage.getItem('isFirst');
  }


  public saveNotiReceive(isFirst) {
    return window.localStorage.setItem('notireceive', isFirst);
  }

  public getNotiReceive() {
    return window.localStorage.getItem('notireceive');
  }

  public clearLocalstorage()
  {
    return window.localStorage.clear()
  }


}
