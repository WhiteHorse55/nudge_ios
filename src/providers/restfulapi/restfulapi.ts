import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { UrlService } from "../../Constant/UrlService";

/*
  Generated class for the RestfulapiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestfulapiProvider {
  constructor(public http: Http) {
    console.log("Hello RestfulapiProvider Provider");
  }

  public getNewMessages() {
    return new Promise((resolve, reject) => {
      let url = UrlService.GET_NEW_MESSAGE;
      this.sendPost(url, "")
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }



  // category
  // custom category in main page
  public getCategory(userid) {
    return new Promise((resolve, reject) => {
      let url = UrlService.GET_CATEGORY;
      let postdata = "userid=" + userid
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }


  public addCategory(userid, ca_name) {
    return new Promise((resolve, reject) => {
      let url = UrlService.ADD_CATEGORY;
      let postdata = "userid=" + userid + "&ca_name=" + ca_name;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  public addUserCategory(ca_id, u_id)
  {
    return new Promise((resolve, reject) => {
      let url = UrlService.ADD_USERCATEGORY;
      let postdata = "userid=" + u_id + "&ca_id=" + ca_id;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  public deleteUserCategory(ca_id, u_id)
  {
    return new Promise((resolve, reject) => {
      let url = UrlService.DELETE_USERCATEGORY;
      let postdata = "userid=" + u_id + "&ca_id=" + ca_id;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  public editCategory(userid, ca_id, ca_name) {
    return new Promise((resolve, reject) => {
      let url = UrlService.EDIT_CATEGORY;
      let postdata =
        "userid=" + userid + "&ca_id=" + ca_id + "&ca_name=" + ca_name;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  public deleteCategory(userid, ca_id) {
    return new Promise((resolve, reject) => {
      let url = UrlService.DELETE_CATEGORY;
      let postdata = "userid=" + userid + "&ca_id=" + ca_id;

      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  //categroy end

  // message

  public getmessage(ca_id, userid) {
    return new Promise((resolve, reject) => {
      let url = UrlService.GET_MESSAGES;
      let postdata = "ca_id=" + ca_id + "&userid=" + userid;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  public addmessage(ca_id, active, message, u_id) {
    return new Promise((resolve, reject) => {
      let url = UrlService.ADD_MESSAGES;
      let postdata =
        "ca_id=" +
        ca_id +
        "&message=" +
        message +
        "&active=" +
        active +
        "&userid=" +
        u_id;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  public editmessage(ca_id, active, message, u_id, messageid) {
    return new Promise((resolve, reject) => {
      let url = UrlService.EDIT_MESSAGES;
      let postdata =
        "ca_id=" +
        ca_id +
        "&message=" +
        message +
        "&active=" +
        active +
        "&userid=" +
        u_id +
        "&messageid=" + messageid;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  public deletemessage(userid, messageid) {
    return new Promise((resolve, reject) => {
      let url = UrlService.DELETE_MESSAGES;
      let postdata =
        "&userid=" +
        userid +
        "&messageid=" + messageid;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  // message end

  public checkEmailExist(email) {
    return new Promise((resolve, reject) => {
      let url = UrlService.CHECK_EMAILEXIST;
      let postdata = "email=" + email;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  // saveusertoken
  public saveUserToken(userid, token)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.SAVE_TOKEN
      let postdata = "token=" + token + "&userid=" + userid;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    })
  }

  // user signin
  public signin(usermail, userpassword) {
    return new Promise((resolve, reject) => {
      let url = UrlService.USER_SIGNIN;
      let postdata = "email=" + usermail + "&password=" + userpassword;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    });
  }

  // user signup
  public signup(info) {
    return new Promise((resolve, reject) => {
      let url = UrlService.USER_SIGNUP;
      let postdata =
        "firstname=" +
        info.firstname +
        "&lastname=" +
        info.lastname +
        "&password=" +
        info.password +
        "&email=" +
        info.email +
        "&photo=" +
        info.photo +
        "&age=" +
        info.age +
        "&timezone=" +
        info.timezone +
        "&gender=" +
        info.gender;

      this.sendPost(url, postdata)
        .then(res => {
          console.log(res);
          resolve(res);
        })
        .catch(er => {
          console.log("this is err", er);
          reject(er);
        });
    });
  }

  public updateprofile(info) {
    return new Promise((resolve, reject) => {
      let url = UrlService.UPDATE_PROFILE;
      let postdata =
        "firstname=" +
        info.firstname +
        "&lastname=" +
        info.lastname +
        "&photo=" +
        info.photo +
        "&email=" +
        info.email +
        "&age=" +
        info.age +
        "&timezone=" +
        info.timezone +
        "&gender=" +
        info.gender;;

        console.log(postdata);

      this.sendPost(url, postdata)
        .then(res => {
          console.log(res);
          resolve(res);
        })
        .catch(er => {
          console.log("this is err", er);
          reject(er);
        });
    });
  }

  // favorite function

  public getFavorite(myid)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.GET_FAVORITE
      let postdata = "myid=" + myid;
      this.sendPost(url, postdata)
      .then(res => {
        console.log(res);
        resolve(res);
      })
      .catch(er => {
        console.log("this is err", er);
        reject(er);
      });
    })
  }


  public addFavorite(m_id, u_id)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.ADD_FAVORITE
      let postdata = "m_id=" + m_id + "&u_id=" + u_id;
      this.sendPost(url, postdata)
      .then(res => {
        console.log(res);
        resolve(res);
      })
      .catch(er => {
        console.log("this is err", er);
        reject(er);
      });
    })
  }

  public deleteFavorite(m_id, u_id)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.DELETE_FAVORITE;
      let postdata = "fa_id=" + m_id + "&u_id=" + u_id;
      this.sendPost(url, postdata)
      .then(res => {
        resolve(res);
      })
      .catch(er => {
        reject(er);
      });
    })
  }


  public checkfavorite(m_id, u_id)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.CHECK_FAVORITE_STATUS
      let postdata = "m_id=" + m_id + "&u_id=" + u_id;
      this.sendPost(url, postdata)
      .then(res => {
        console.log(res);
        resolve(res);
      })
      .catch(er => {
        console.log("this is err", er);
        reject(er);
      });
    })
  }


  // getNotificationList()
  public getNotificationList(u_id)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.GET_NOTIFICATION
      let postdata = "userid=" + u_id;
      this.sendPost(url, postdata)
      .then(res => {
        console.log(res);
        resolve(res);
      })
      .catch(er => {
        console.log("this is err", er);
        reject(er);
      });
    })
  }

  public sendNotificationSeen(noti_id)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.SEEN_NOTIFICATION
      let postdata = "noti_id=" + noti_id;
      this.sendPost(url, postdata)
      .then(res => {
        console.log(res);
        resolve(res);
      })
      .catch(er => {
        console.log("this is err", er);
        reject(er);
      });
    })
  }

  public deleteNotification(noti_id)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.DELETE_NOTIFICATION
      let postdata = "noti_id=" + noti_id;
      this.sendPost(url, postdata)
      .then(res => {
        console.log(res);
        resolve(res);
      })
      .catch(er => {
        console.log("this is err", er);
        reject(er);
      });
    })
  }

  // send forgot password request
  public sendForgotPassword(email)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.FORGOT_PASSWORD
      let postdata = "email=" + email
      this.sendPost(url, postdata)
      .then(res => {
        console.log(res);
        resolve(res);
      })
      .catch(er => {
        console.log("this is err", er);
        reject(er);
      });
    })
  }

//
  private sendPost(url, postdata) {
    return new Promise((resolve, reject) => {
      var headers = new Headers();
      headers.append(
        "Content-Type",
        "application/x-www-form-urlencoded; charset=UTF-8"
      );
      this.http.post(url, postdata, { headers: headers }).subscribe(
        result => {
          resolve(result.json());
        },
        err => {
          reject(err);
        }
      );
    });
  }

  private sendGetInfo(url, info = null) {
    return new Promise((resolve, reject) => {
      this.http.get(url, {}).subscribe(
        res => {
          console.log("this is result", res);
          resolve(res.json());
        },
        err => {
          console.log("this is error", err);
          reject(err);
        }
      );
    });
  }
}
