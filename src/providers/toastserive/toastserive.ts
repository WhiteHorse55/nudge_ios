import { Injectable } from '@angular/core';
import { Toast, ToastController } from 'ionic-angular';

/*
  Generated class for the ToastseriveProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ToastseriveProvider {

  toast: Toast;
  constructor(public toastCtrl : ToastController) {
    console.log('Hello ToastseriveProvider Provider');
  }

  create(message, ok = false, duration = 2000) {

    if (this.toast) {
      this.toast.dismiss();
    }

    this.toast = this.toastCtrl.create({
      message,
      duration: ok ? null : duration,
      position: "bottom",
      showCloseButton: ok,
      closeButtonText: "OK"
    });
    this.toast.present();
  }

}
