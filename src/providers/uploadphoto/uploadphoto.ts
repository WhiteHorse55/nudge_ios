import { UrlService } from './../../Constant/UrlService';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { Constant } from '../../Constant/Constant';

/*
  Generated class for the UploadphotoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UploadphotoProvider {

  constructor(public http: HttpClient, public transfer : FileTransfer) {
    console.log('Hello UploadphotoProvider Provider');
  }

    // upload image
    uploadPhoto(info) {
      return new Promise((resolve, reject) => {
        const fileTransfer: FileTransferObject = this.transfer.create();

        let filename = Constant.createFileName();
        let options1: FileUploadOptions = {
          fileKey: "c_image",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params: { fileName: filename },
          headers: {}
        };
        fileTransfer
          .upload(info, UrlService.UPLOAD_PHOTO, options1)
          .then(data => {
            console.log("this is result upload", data);
            resolve(filename);
          })
          .catch(err => {
            reject(err);
            console.log("this is upload error", err);
          });
      });
    }

}
