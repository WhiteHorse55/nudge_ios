import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseMessage } from '../../Constant/ResponseMessage';

/*
  Generated class for the ValidationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ValidationProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ValidationProvider Provider');
  }

  signinValidation(info)
  {
    return new Promise((resolve, reject)=>{
        if(info.email == "")
        {
          reject(ResponseMessage.EMPTY_EMAIL)
        }else if(info.password == "")
        {
          reject(ResponseMessage.EMPTY_PASSWORD)
        }else{
          resolve(true)
        }
    })
  }

}
