import { UrlService } from './../../Constant/UrlService';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
/*
  Generated class for the PushserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PushserviceProvider {

  constructor(public http: Http) {
    console.log('Hello PushserviceProvider Provider');
  }

  updateTokenInfo(userid, token)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.SAVE_TOKEN
      let postdata = "token=" + token + "&userid=" + userid;
      this.sendPost(url, postdata)
        .then(res => {
          resolve(res);
        })
        .catch(er => {
          reject(er);
        });
    })
  }

  sendPushNotificationRequest(ring, vibration, usertoken)
  {
    return new Promise((resolve, reject)=>{
      let url = UrlService.SEND_NOTIFICATION
      let postdata = "ring=" + ring + "&vibration=" + vibration + "&usertoken=" + usertoken
      this.sendPost(url, postdata)
      .then(res => {
        console.log(res);
        resolve(res);
      })
      .catch(er => {
        console.log("this is err", er);
        reject(er);
      });
    })
  }


  private sendPost(url, postdata) {
    return new Promise((resolve, reject) => {
      var headers = new Headers();
      headers.append(
        "Content-Type",
        "application/x-www-form-urlencoded; charset=UTF-8"
      );
      this.http.post(url, postdata, { headers: headers }).subscribe(
        result => {
          resolve(result.json());
        },
        err => {
          reject(err);
        }
      );
    });
  }

}
