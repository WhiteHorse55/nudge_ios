import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddcategoryPage } from './addcategory';

@NgModule({
  declarations: [
    AddcategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(AddcategoryPage),
    ComponentsModule
  ],
})
export class AddcategoryPageModule {}
