import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";
import { CustomalertProvider } from "../../providers/customalert/customalert";
import { Constant } from "../../Constant/Constant";
import { ResponseMessage } from "../../Constant/ResponseMessage";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { LoadingProvider } from "../../providers/loading/loading";

/**
 * Generated class for the AddcategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-addcategory",
  templateUrl: "addcategory.html"
})
export class AddcategoryPage {
  isback: any;
  // edit
  category_name: any;
  category_id: any;

  edit_type: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toastservice: ToastseriveProvider,
    public alertctrl: CustomalertProvider,
    public restfulapi: RestfulapiProvider,
    public localstorageprovider: LocalstorageProvider,
    public loadingprovider: LoadingProvider
  ) {
    this.category_name = "";
    this.isback = true;

    this.edit_type = this.navParams.get("type");
    if (this.edit_type == "edit") {
      this.category_name = this.navParams.get("name");
      this.category_id = this.navParams.get("ca_id");
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AddcategoryPage");
  }

  // send add category request
  onclickdismiss() {
    this.loadingprovider.showLoadingView();
    if (this.category_name == "") {
      this.loadingprovider.removeLoadingView();
      this.alertctrl.presentErrorAlert(ResponseMessage.EMPTY_CATEGORY);
    } else {
      if (this.edit_type == "edit") {
        this.editCategory();
      } else {
        this.addCategory();
      }
    }
  }

  editCategory() {
    let userid = this.getuserinfo()["id"];

    this.restfulapi
      .editCategory(userid, this.category_id, this.category_name)
      .then(res => {
        this.loadingprovider.removeLoadingView();
        if (res["status"] == ResponseMessage.RESPONSE_SUCCESS) {
          this.toastservice.create(res["msg"]);
          this.viewCtrl.dismiss(this.category_name);
        } else {
          this.toastservice.create(res["msg"]);
        }
      })
      .catch(er => {
        this.loadingprovider.removeLoadingView();
      });
  }

  addCategory() {
    let userid = this.getuserinfo()["id"];
    this.restfulapi
      .addCategory(userid,this.category_name)
      .then(res => {
        this.loadingprovider.removeLoadingView();
        if (res["status"] == ResponseMessage.RESPONSE_SUCCESS) {
          this.toastservice.create(res["msg"]);
          this.viewCtrl.dismiss(this.category_name);
        } else {
          this.toastservice.create(res["msg"]);
        }
      })
      .catch(er => {
        this.loadingprovider.removeLoadingView();
      });
  }

  onclickclose() {
    this.category_name = "";
    this.viewCtrl.dismiss(this.category_name);
  }

  getuserinfo() {
    return this.localstorageprovider.getUserinfo();
  }
}
