import { Component, Renderer } from "@angular/core";
import { IonicPage, NavController, NavParams, ViewController } from "ionic-angular";

/**
 * Generated class for the CustomimagedialogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-customimagedialog",
  templateUrl: "customimagedialog.html"
})
export class CustomimagedialogPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public render : Renderer
  ) {
    this.render.setElementClass(
      viewCtrl.pageRef().nativeElement,
      "custom-popup",
      true
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CustomimagedialogPage");
  }

  onclickdismissbutton()
  {
    this.viewCtrl.dismiss()
  }

  onclickuploadbutton()
  {
    this.viewCtrl.dismiss()
  }

}
