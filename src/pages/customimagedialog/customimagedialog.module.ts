import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomimagedialogPage } from './customimagedialog';

@NgModule({
  declarations: [
    CustomimagedialogPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomimagedialogPage),
  ],
})
export class CustomimagedialogPageModule {}
