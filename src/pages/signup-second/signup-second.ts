import { ResponseMessage } from './../../Constant/ResponseMessage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { PasswordValidation } from '../../Constant/PasswordValidation';
import { ToastseriveProvider } from '../../providers/toastserive/toastserive';

/**
 * Generated class for the SignupSecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup-second',
  templateUrl: 'signup-second.html',
})
export class SignupSecondPage  implements OnInit{

  isback : boolean
  passwordform : FormGroup
  userdata : any
  isChecked : boolean

  // confirmpassword flag
  matchedPasswords : boolean
  // email get from first page
  passdata_email : any

  constructor(public navCtrl: NavController, public navParams: NavParams,public fb : FormBuilder, public toastservice : ToastseriveProvider) {
    this.isback = true
    this.isChecked = true
    this.matchedPasswords = false
    this.userdata  = {password : "",confirmpassword : ""}

    this.passdata_email = this.navParams.get('email')

  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.passwordform = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'), Validators.minLength(6)]),
      confirmpassword : new FormControl('',[Validators.required])
    });

  }

  onchangevalue()
  {
    if(this.userdata.password == this.userdata.confirmpassword)
    {
      this.matchedPasswords = true
    }else{
      this.matchedPasswords = false
    }
  }



  ionViewDidLoad() {

  }

  submitform()
  {
    let info = {email : this.passdata_email, password : this.userdata.password}
    console.log("this is log info", info);

    if(this.matchedPasswords == false)
    {
      this.toastservice.create(ResponseMessage.INVALID_REENTER)
    }else{
      this.navCtrl.push('SignupprofilePage',{info : info})
    }

  }

  onclickBackbutton()
  {
    this.navCtrl.pop()
  }

}
