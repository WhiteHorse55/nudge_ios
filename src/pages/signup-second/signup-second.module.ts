import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupSecondPage } from './signup-second';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SignupSecondPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupSecondPage),
    ComponentsModule
  ],
})
export class SignupSecondPageModule {}
