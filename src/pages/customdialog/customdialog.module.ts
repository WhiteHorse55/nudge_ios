import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomdialogPage } from './customdialog';

@NgModule({
  declarations: [
    CustomdialogPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomdialogPage),
    ComponentsModule
  ],
})
export class CustomdialogPageModule {}
