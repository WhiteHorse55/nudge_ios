import { Component, Renderer } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";

/**
 * Generated class for the CustomdialogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-customdialog",
  templateUrl: "customdialog.html"
})
export class CustomdialogPage {

  randomNameArray = []
  randomName : any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public render: Renderer,
    public viewCtrl: ViewController
  ) {
    this.render.setElementClass(viewCtrl.pageRef().nativeElement, 'custom-popup', true);
    this.randomNameArray = ["Sunshine","John","Jack","Max","Luis","Jhonson"]
    this.randomName = this.generateRandomName()
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CustomdialogPage");
  }

  onclickdismissbutton()
  {
    this.viewCtrl.dismiss()
  }

  onclickRefreshbutton()
  {
    this.randomName = this.generateRandomName()
  }

  generateRandomName()
  {
    var value = this.randomNameArray[Math.floor(Math.random() * this.randomNameArray.length)]
    return value
  }
}
