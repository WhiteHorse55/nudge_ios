import { AppRate } from '@ionic-native/app-rate';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ActionSheetController, App } from "ionic-angular";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { EmailComposer } from "@ionic-native/email-composer";

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-setting",
  templateUrl: "setting.html"
})
export class SettingPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public localstorage: LocalstorageProvider,
    public emailcomposer:EmailComposer,
    public actionSheetCtrl : ActionSheetController,
    public app : App,
    public apprate : AppRate
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad SettingPage");
  }

  onclicknotification()
  {
    this.navCtrl.push('NotificationsettingPage')
  }

  onclicksharebutton()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select You can change world by sharing",
      buttons: [
        {
          text: "Facebook",
          handler: () => {}
        },
        {
          text: "Google",
          handler: () => {}
        },

        {
          text: "Instgram",
          handler: () => {}
        },

        {
          text: "Twitter",
          handler: () => {}
        },

        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });

    actionSheet.present();
  }

  onclickrateapp()
  {

    // this.apprate.preferences = {

    //   useLanguage : 'en',
    //   displayAppName : "Nudge",
    //   usesUntilPrompt: 2,
    //   promptAgainForEachNewVersion : true,
    //   // inAppReview : true,
    //   storeAppURL: {
    //    android: 'market://details?id=com.getditto',
    //   },
    //   customLocale : {
    //     title : "Do you enjoy %@",
    //     message : 'If you enjoy %@. Would you mind talking a moment to rate it',
    //     cancelButtonLabel : 'No, Thanks',
    //     laterButtonLabel : "Remain me later",
    //     rateButtonLabel : "Rate it now"
    //   },
    // };

    // this.apprate.promptForRating(true);

    this.apprate.preferences.storeAppURL = {
      ios: '<app_id>',
      android: 'market://details?id=com.synpase.nudge',
    };

    this.apprate.promptForRating(true);
  }

  onclickuserprofile()
  {
      this.navCtrl.push('SignupprofilePage', {from : 'setting'})
  }

  onclickcategory()
  {
    this.navCtrl.push('SelectcategorPage')
  }

  onclicksignout() {
    this.localstorage.clearLocalstorage()
    this.app.getRootNav().setRoot('SigninPage')
    // this.navCtrl.setRoot()
  }

  onclickFeedback()
  {
    let email = {
      to: 'hey@thenudge.club',
      cc: [],
      bcc: [],
      attachment: [],
      subject: 'Report Content',
      body: ""
    };

    this.emailcomposer.addAlias('gmail', 'com.google.android.gm');
    this.emailcomposer.addAlias('outlook', 'com.microsoft.android.outlook');
    this.emailcomposer.open(email);

    this.emailcomposer
      .isAvailable()
      .then((available: boolean) => {
        console.log('this is available', available);
        if (available) {
          //Now we know we can send
          // Send a text message using default options
          this.emailcomposer.open(email);
        }
      })
      .catch(err => {
        console.log('====================================');
        console.log(err);
        console.log('====================================');
      });
  }

  onclicksecurity()
  {
    this.navCtrl.push("ForgotpassPage");
  }

}
