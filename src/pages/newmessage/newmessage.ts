import { ResponseMessage } from './../../Constant/ResponseMessage';
import { Component, Renderer, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  ActionSheetController,
  Slides
} from "ionic-angular";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { SocialSharing } from "@ionic-native/social-sharing";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { LoadingProvider } from "../../providers/loading/loading";

/**
 * Generated class for the NewmessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-newmessage",
  templateUrl: "newmessage.html"
})
export class NewmessagePage {

  current_index : any
  message_data : any

  @ViewChild('mySlider') slider:  Slides;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public render: Renderer,
    public viewCtrl: ViewController,
    public restfulapi : RestfulapiProvider,
    public socialshare : SocialSharing,
    public actionsheetctrl : ActionSheetController,
    public toastctrl : ToastseriveProvider,
    public localstorage : LocalstorageProvider,
    public loadingprovider : LoadingProvider
  ) {
    this.render.setElementClass(
      viewCtrl.pageRef().nativeElement,
      "custom-share",
      true
    );
    this.message_data = []
    this.current_index = 0

    setTimeout(() => {
      if(this.viewCtrl)
      {
        this.viewCtrl.dismiss();
      }
			
		}, 15000);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad NewmessagePage");
    this.getNewMessages()
  }

  getNewMessages()
  {
    this.restfulapi.getNewMessages().then(res=>{
        this.message_data = res['data']
    }).catch(er=>{

    })
  }

  onSlideChange()
  {
    this.current_index = this.slider.getActiveIndex()  -  1
  }

  onclickdismissbutton()
  {
    this.viewCtrl.dismiss()
  }

  onclicklike()
  {
    let info = this.message_data[this.current_index];
    let myid = this.localstorage.getuserID()
    let messsage_id = info['id']
    this.loadingprovider.showLoadingView()
    this.restfulapi.checkfavorite(messsage_id, myid).then(res=>{
      if(res['status'] ==  ResponseMessage.RESPONSE_SUCCESS)
      {
        this.restfulapi.addFavorite(messsage_id, myid).then(res=>{
          this.loadingprovider.removeLoadingView()
          if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
          {
            this.toastctrl.create(res['msg'])
          }else{
            this.toastctrl.create(res['msg'])
          }

        }).catch(er=>{
          this.loadingprovider.removeLoadingView()
        })
      }else{
        this.loadingprovider.removeLoadingView()
        this.toastctrl.create(res['msg'])
      }
    }).catch(er=>{
      this.loadingprovider.removeLoadingView()
    })
  }
  public presentActionSheet() {


    let message = this.message_data[this.current_index]['message']
    console.log("message=>", message);

    let actionSheet = this.actionsheetctrl.create({
      title: "Select You can change world by sharing",
      buttons: [
        {
          text: "Facebook",
          handler: () => {
            this.sharewithfacebook(message)
          }
        },

        {
          text: "Instgram",
          handler: () => {
            this.sharewithinstgram(message)
          }
        },

        {
          text: "Twitter",
          handler: () => {
            this.sharewithtwitter(message)
          }
        },

        {
          text: "Other",
          handler: () => {
            this.shareother(message)
          }
        },

        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });

    actionSheet.present();
  }

  public sharewithtwitter(message) {
    console.log("this is share message", message)
    this.socialshare
      .shareViaTwitter(message,"","")
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install Twitter app first!");
      });
  }

  public sharewithinstgram(message) {
    this.socialshare
      .shareViaInstagram(message, "")
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install instagram app first!");
      });
  }

  public sharewithfacebook(message) {

    // this.socialshare.shareViaFacebookWithPasteMessageHint(this.category.message)
    this.socialshare
      .shareViaFacebook(message, message, message)
      .then(res => {
        console.log("this is result",res)
      })
      .catch(er => {
        this.toastctrl.create("Please install facebook app first!");
      });

  }

  public shareother(message)
  {
    this.socialshare.share(message)
  }
}
