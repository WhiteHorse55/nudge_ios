import { ToastseriveProvider } from './../../providers/toastserive/toastserive';
import { Constant } from './../../Constant/Constant';
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  ModalController,
  Events,
  App
} from "ionic-angular";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { ResponseMessage } from '../../Constant/ResponseMessage';
import { LocalstorageProvider } from '../../providers/localstorage/localstorage';
import { modelGroupProvider } from '@angular/forms/src/directives/ng_model_group';

/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-main",
  templateUrl: "main.html"
})
export class MainPage {
  category_array: any;
  isEditable : boolean
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl : ModalController,
    public restfulapi : RestfulapiProvider,
    public toastctrl : ToastseriveProvider,
    public localstorageprovider : LocalstorageProvider,
    public event : Events,
    public app : App
  ) {
    this.category_array = [];
    this.isEditable = false

    this.event.subscribe('noti_receive',()=>{
      this.checknewNotificationStatus()
    })
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainPage");
    // this.generateDataArray();
    let loginstatus = this.localstorageprovider.getLoginStatus()
    // this.showAppMessageScreen()
    if(loginstatus)
    {

    }else{
      this.showAppMessageScreen()
    }

   

  }

  showrandomcategorypage()
  {
    let modal = this.modalCtrl.create('SelectcategorPage')
    modal.onDidDismiss(res=>{
      this.showNotificationSettingpage()
    })
    modal.present()
  }


  showNotificationSettingpage()
  {
    this.navCtrl.push('NotificationsettingPage')
  }

  showAppMessageScreen()
  {
    let modal = this.modalCtrl.create('GetstartedPage',{} ,{showBackdrop:true, enableBackdropDismiss:true, cssClass : 'inset-modal'});
    modal.onDidDismiss(res=>{
        console.log("on dismiss event");
        this.localstorageprovider.saveLoginStatus(true)
        this.showrandomcategorypage()
    })
    modal.present();
  }


  ionViewWillEnter(){
    this.getcategorys()
  }

  checknewNotificationStatus()
  {
    let status = this.localstorageprovider.getNotiReceive()
    if(status == 'true')
    {
      let modal = this.modalCtrl.create('NewmessagePage',{} ,{showBackdrop:true, enableBackdropDismiss:true, cssClass : 'inset-modal'});
      modal.onDidDismiss(res=>{
          console.log("on dismiss event");
          this.localstorageprovider.saveNotiReceive(false)
      })
      modal.present();
    }
  }



  getcategorys()
  {
    let userid = this.localstorageprovider.getuserID()
    this.restfulapi.getCategory(userid).then(res=>{
      // console.log("this is for test ", res)
      if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
      {
        this.category_array = res['data']['all']
      }else{
        this.toastctrl.create(res['status'])
      }
    }).catch(er=>{
      this.toastctrl.create(er)
    })
  }

  onclickItem(index)
  {
    this.isEditable = false
    let temp = this.category_array[index]
    this.navCtrl.push('CustomcategoryPage',{category_name : temp.ca_name, ca_id : temp.ca_id})
  }

  onclickEdit($event)
  {
    this.isEditable = !this.isEditable
  }



  onClickEditOperation(index)
  {
    let popover = this.popoverCtrl.create("PopoverPage");
    popover.onDidDismiss(res=>{

      if(res != null)
      {
        let info = this.category_array[index]
        if(res.type == 'edit')
        {
          let modalview = this.modalCtrl.create('AddcategoryPage', {type : "edit", name : info.ca_name, ca_id : info.ca_id})
          modalview.onDidDismiss(res=>{
            this.getcategorys()
          })
          modalview.present()
        }else{
          console.log("clicked delete button")
          this.restfulapi.deleteCategory(this.getuserinfo()['id'],info.ca_id).then(res=>{

            if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
            {
              this.getcategorys()
            }else{
              this.toastctrl.create(res['msg'])
            }
          }).catch(er=>{
            this.toastctrl.create(er)
          })
        }
      }

    })
    popover.present({
      ev: index
    });
  }

  onclickFabButton(){
      let modalview = this.modalCtrl.create('AddcategoryPage', {type : "create"})
      modalview.onDidDismiss(res=>{
         this.getcategorys()
      })
      modalview.present()
  }

  getuserinfo() {
    return this.localstorageprovider.getUserinfo();
  }

  onclicklongpress(index)
  {
    console.log("onclick long press", index);

  }
}
