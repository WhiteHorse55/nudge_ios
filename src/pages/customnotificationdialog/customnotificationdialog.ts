import { Component, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the CustomnotificationdialogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-customnotificationdialog',
  templateUrl: 'customnotificationdialog.html',
})
export class CustomnotificationdialogPage {

  message : any
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public render :Renderer) {
    this.render.setElementClass(
      viewCtrl.pageRef().nativeElement,
      "custom-popup",
      true
    );
    this.message = this.navParams.get('message');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomnotificationdialogPage');
  }

  onclickdismissbutton()
  {
    this.viewCtrl.dismiss()
  }
}
