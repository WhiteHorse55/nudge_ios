import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomnotificationdialogPage } from './customnotificationdialog';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CustomnotificationdialogPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomnotificationdialogPage),
    ComponentsModule
  ],
})
export class CustomnotificationdialogPageModule {}
