import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";
import { ResponseMessage } from "../../Constant/ResponseMessage";
import { LoadingProvider } from "../../providers/loading/loading";

/**
 * Generated class for the ForgotpassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-forgotpass",
  templateUrl: "forgotpass.html"
})
export class ForgotpassPage {
  isback: any;
  email : any;
  isregisteredmail : boolean
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restfulapi: RestfulapiProvider,
    public toastservice : ToastseriveProvider,
    public loadingctrl : LoadingProvider
  ) {
    this.isback = true;
    this.isregisteredmail = false
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ForgotpassPage");
  }

  onclickBackbutton() {
    this.navCtrl.pop();
  }

  checkRegisteredEmail() {
    this.loadingctrl.showLoadingView()
    this.restfulapi.sendForgotPassword(this.email).then(res=>{
      this.loadingctrl.removeLoadingView()
      if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
      {
        this.navCtrl.pop()
        this.isregisteredmail = true
        this.toastservice.create(res['message'])

      }else{
        this.email = ""
        this.toastservice.create(res['message'])
        this.isregisteredmail = false
      }

    }).catch(er=>{
      this.email = ""
      this.loadingctrl.removeLoadingView()
      this.toastservice.create(er)
      this.isregisteredmail = false
    })
  }
}
