import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupprofilePage } from './signupprofile';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SignupprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(SignupprofilePage),
    ComponentsModule
  ],
})
export class SignupprofilePageModule {}
