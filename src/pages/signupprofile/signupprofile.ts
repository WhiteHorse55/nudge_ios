import { ToastseriveProvider } from './../../providers/toastserive/toastserive';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ModalController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { UploadphotoProvider } from '../../providers/uploadphoto/uploadphoto';
import { RestfulapiProvider } from '../../providers/restfulapi/restfulapi';
import { LoadingProvider } from '../../providers/loading/loading';
import { ResponseMessage } from '../../Constant/ResponseMessage';
import { LocalstorageProvider } from '../../providers/localstorage/localstorage';
import { UrlService } from '../../Constant/UrlService';
/**
 * Generated class for the SignupprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signupprofile',
  templateUrl: 'signupprofile.html',
})
export class SignupprofilePage {

  isback : boolean
  base64Image : string
  username : any

  myform : FormGroup
  isDone : any
  isProfileimage : any

  // data from previous pages
  passdata : any
  userinfo : any
  image_url : string

  gender : string
  timezone : string
  age : string

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastservice : ToastseriveProvider,
              public actionSheetCtrl : ActionSheetController,
              public camera : Camera,
              public modalCtrl : ModalController,
              public uploadprovider : UploadphotoProvider,
              public restfulprovider: RestfulapiProvider,
              public loadingprovider : LoadingProvider,
              public localstorage : LocalstorageProvider) {

    this.isback = true
    this.base64Image = ""
    this.username = {firstname : "", lastname : ""}
    this.isDone = false
    this.isProfileimage = false
    this.image_url = UrlService.SERVER_IMAGE_URL

    this.passdata = this.navParams.get('info')
    this.userinfo = this.localstorage.getUserinfo()

    let from = this.navParams.get('from')
    if(from == 'setting' && from != null)
    {
      this.isDone = true
      this.isProfileimage = true
      console.log("this is userinfo",this.userinfo['photo']);
    }

    this.gender = ""
    this.timezone = ""
    this.age = ""

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad SignupprofilePage');
    if(this.isDone == false)
    {
      this.openRandomNameDialog()
    }

  }

  onclickBackbutton()
  {
    this.navCtrl.pop()
  }

  openRandomNameDialog()
  {
    let modal = this.modalCtrl.create('CustomdialogPage',{} ,{showBackdrop:true, enableBackdropDismiss:true, cssClass : 'inset-modal'});

    modal.onDidDismiss(res=>{
      this.openPhotoDialog()
    })
    modal.present();
  }

  openPhotoDialog()
  {
    let modal = this.modalCtrl.create('CustomimagedialogPage',{} ,{showBackdrop:true, enableBackdropDismiss:true, cssClass : 'inset-modal'});
    modal.present();
  }

  onclickupdatebutton()
  {
    console.log(this.userinfo);

    let email = this.localstorage.getUserinfo()['email']
    console.log("this is test", email)
    if(this.userinfo.firstname == '' || this.userinfo.lastname == '')
    {
      this.loadingprovider.removeLoadingView()
      this.toastservice.create('Please fill name info', false, 2000)
    }else{
      if(this.base64Image == '')
      {
        
        let info = {
          email : email,
          firstname : this.userinfo.firstname,
          lastname :  this.userinfo.lastname,
          age : this.userinfo.age,
          gender : this.userinfo.gender,
          timezone : this.userinfo.timezone,
          photo : ""}

          console.log("this is test", info);

        this.restfulprovider.updateprofile(info).then(re=>{
          this.loadingprovider.removeLoadingView()
          if(re['status'] = ResponseMessage.RESPONSE_SUCCESS)
          {
            console.log('re',re)
            this.localstorage.saveUserInfo(re['data'])
            this.navCtrl.pop()
          }else{
            this.toastservice.create(re['msg'])
          }

        }).catch(err=>{
          this.loadingprovider.removeLoadingView()
        })
      }else{
        this.uploadprovider.uploadPhoto(this.base64Image).then(res=>{
          let info = {
            email : email,
            firstname : this.userinfo.firstname,
            lastname :  this.userinfo.lastname,
            age : this.userinfo.age,
            gender : this.userinfo.gender,
            timezone : this.userinfo.timezone,
            photo : res}
            console.log("this is testwwww", info);
          this.restfulprovider.updateprofile(info).then(re=>{
            this.localstorage.saveUserInfo(re['data'])
            this.loadingprovider.removeLoadingView()
            console.log('re',re)
            this.navCtrl.pop()
          }).catch(err=>{
            this.loadingprovider.removeLoadingView()
          })

        }).catch(er=>{
          this.loadingprovider.removeLoadingView()
          this.toastservice.create("upload photo failed")
        })
      }
    }
  }

  // click done button
  onclickdonebutton()
  {
    console.log("this is click event");
    this.loadingprovider.showLoadingView()
    if(this.username.firstname == '' || this.username.lastname == '')
    {
      this.loadingprovider.removeLoadingView()
      this.toastservice.create('Please fill name info', false, 2000)
    }else{

      if(this.base64Image == '')
      {
        let info = {email : this.passdata.email,
          password : this.passdata.password,
          firstname : this.username.firstname,
          lastname :  this.username.lastname,
          age : this.age,
          gender : this.gender,
          timezone : this.timezone,
          photo : ""}
        this.restfulprovider.signup(info).then(re=>{
          this.loadingprovider.removeLoadingView()
          if(re['status'] = ResponseMessage.RESPONSE_SUCCESS)
          {
            // this.localstorage.saveUserInfo(info)
            this.navCtrl.push('TermservicePage')
          }else{
            this.toastservice.create(re['msg'])
          }

        }).catch(err=>{
          this.loadingprovider.removeLoadingView()
        })
      }else{
        this.uploadprovider.uploadPhoto(this.base64Image).then(res=>{
          let info = {email : this.passdata.email,
            password : this.passdata.password,
            firstname : this.username.firstname,
            lastname :  this.username.lastname,
            age : this.age,
            gender : this.gender,
            timezone : this.timezone,
            photo : res}

          this.restfulprovider.signup(info).then(re=>{
            // this.localstorage.saveUserInfo(info)
            this.navCtrl.push('TermservicePage')
            this.loadingprovider.removeLoadingView()
          }).catch(err=>{
            this.loadingprovider.removeLoadingView()
          })

        }).catch(er=>{
          this.loadingprovider.removeLoadingView()
          this.toastservice.create("upload photo failed")
        })
      }

    }
  }

   ////////////////////////////show take photo actionsheet///////////////////////////////////////////////
   public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select Image Source",
      buttons: [
        {
          text: "Load from Library",
          handler: () => {
            this.takePhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Load from Camera",
          handler: () => {
            this.takePhoto(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });

    actionSheet.present();
  }

  private takePhoto(sourceType) {
    const options: CameraOptions = {
      quality: 100, // picture quality
      targetHeight: 300,
      targetWidth: 200,
      correctOrientation : true,
      allowEdit : true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.isProfileimage = false
        // this.base64Image = imageData;
      },
      err => {
        console.log(err);
      }
    );
  }

}
