import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  ModalController
} from "ionic-angular";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { ResponseMessage } from "../../Constant/ResponseMessage";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";
import { LoadingProvider } from "../../providers/loading/loading";
import { CustomalertProvider } from "../../providers/customalert/customalert";

/**
 * Generated class for the CustomcategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-customcategory",
  templateUrl: "customcategory.html"
})
export class CustomcategoryPage {
  isBack: any;
  pass_category: any;
  pass_category_id : any

  customcategoryArray: Array<any>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl : ModalController,
    public restfulapi : RestfulapiProvider,
    public localstorageprovider : LocalstorageProvider,
    public toastctrl : ToastseriveProvider,
    public loadingprovider: LoadingProvider,
    public alertprovider : CustomalertProvider
  ) {
    this.isBack = true;
    this.pass_category = "";
    // this.customcategoryArray = [];
    this.pass_category = this.navParams.get('category_name')
    this.pass_category_id = this.navParams.get('ca_id')
    // this.generateCustomCateogryArray();
    this.getMessagesByCategory()
  }

  getMessagesByCategory()
  {
    this.loadingprovider.showLoadingView()

    this.restfulapi.getmessage(this.pass_category_id,this.getuserinfo()['id']).then(res=>{
      this.loadingprovider.removeLoadingView()

      if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
      {
        console.log(res)
        this.customcategoryArray = res['data']
      }else{
        this.customcategoryArray = []
        this.toastctrl.create(res['msg'])
      }

    }).catch(er=>{
      this.loadingprovider.removeLoadingView()
    })
  }

  getuserinfo() {
    return this.localstorageprovider.getUserinfo();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CustomcategoryPage");
  }

  onclickbackbutton() {
    this.navCtrl.pop();
  }

  onclickFabButton() {
    let modalview = this.modalCtrl.create('AddmessagePage', {type : "create", ca_id : this.pass_category_id})
    modalview.onDidDismiss(res=>{
        this.getMessagesByCategory()
    })
    modalview.present()
  }

  onclickItem(index){
    let info = this.customcategoryArray[index];
    let modalview = this.modalCtrl.create('AddmessagePage', {type : "show",info: info,ca_id : this.pass_category_id})
    modalview.present()
  }



  onClickEditOperation(index) {

    let popover = this.popoverCtrl.create("PopoverPage");
    popover.onDidDismiss(res => {

      if(res != null)
      {
        let info = this.customcategoryArray[index];
        if (res.type == "edit") {
          let modalview = this.modalCtrl.create("AddmessagePage", {
            type: "edit",
            info: info,
            ca_id : this.pass_category_id
          });
          modalview.onDidDismiss(res => {
            this.getMessagesByCategory()
          });
          modalview.present();
        } else {
            this.loadingprovider.showLoadingView()
            this.alertprovider.presentAlertWithCallback("Are you sure delete this message?","").then(res=>{
              this.restfulapi.deletemessage(this.getuserid(), info['id']).then(res=>{
                this.loadingprovider.removeLoadingView()

                if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
                {
                  this.getMessagesByCategory()
                }else{
                  this.toastctrl.create(res['msg'])
                }
              })
            }).catch(er=>{
              this.loadingprovider.removeLoadingView()
            })

        }
      }

    });
    popover.present({
      ev: index
    });
  }

  getuserid()
  {
    return this.localstorageprovider.getuserID()
  }

  onclicksharebutton(index)
  {

  }

  onclicklikebutton(index)
  {
    let info = this.customcategoryArray[index];
    let myid = this.getuserid()
    let messsage_id = info['id']
    this.loadingprovider.showLoadingView()
    this.restfulapi.checkfavorite(messsage_id, myid).then(res=>{
      if(res['status'] ==  ResponseMessage.RESPONSE_SUCCESS)
      {
        this.restfulapi.addFavorite(messsage_id, myid).then(res=>{
          this.loadingprovider.removeLoadingView()
          if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
          {
            this.toastctrl.create(res['msg'])
          }else{
            this.toastctrl.create(res['msg'])
          }

        }).catch(er=>{
          this.loadingprovider.removeLoadingView()
        })
      }else{
        this.loadingprovider.removeLoadingView()
        this.toastctrl.create(res['msg'])
      }
    }).catch(er=>{
      this.loadingprovider.removeLoadingView()
    })

  }
}
