import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomcategoryPage } from './customcategory';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CustomcategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomcategoryPage),
    ComponentsModule
  ],
})
export class CustomcategoryPageModule {}
