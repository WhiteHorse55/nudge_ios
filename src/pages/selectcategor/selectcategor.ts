import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController, ViewController } from "ionic-angular";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { ResponseMessage } from "../../Constant/ResponseMessage";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { Category_Model } from '../../Model/category_model';
import { LoadingProvider } from "../../providers/loading/loading";


/**
 * Generated class for the SelectcategorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */



@IonicPage()
@Component({
  selector: "page-selectcategor",
  templateUrl: "selectcategor.html"
})



export class SelectcategorPage {
  category_array : any
  usercategory:any
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restfulapi: RestfulapiProvider,
    public toastctrl : ToastController,
    public viewctrl : ViewController,
    public localstorage : LocalstorageProvider,
    public loadingctrl : LoadingProvider
  ) {
    this.category_array = []
    this.usercategory = []
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SelectcategorPage");
  }

  onclickdismissbutton()
  {
    this.viewctrl.dismiss()
  }

  ionViewWillEnter() {
    this.getcategorys();
  }

  getcategorys() {
    this.category_array = []
    let userid = this.localstorage.getuserID()
    this.restfulapi
      .getCategory(userid)
      .then(res => {
        console.log("this is for test ", res);
        if (res["status"] == ResponseMessage.RESPONSE_SUCCESS) {
          // this.category_array = res["data"];
          this.generatearray(res['data']['all'], res['data']['user'])
        } else {
          this.toastctrl.create(res["status"]);
        }
      })
      .catch(er => {
        this.toastctrl.create(er);
      });
  }

  generatearray(all, user)
  {

    all.forEach(allelement => {
      let ca_model = new Category_Model()
      ca_model.ca_id = allelement.ca_id
      ca_model.ca_content = allelement.ca_content
      ca_model.ca_icon = allelement.ca_icon
      ca_model.ca_name = allelement.ca_name
      ca_model.u_id = allelement.u_id
      ca_model.approve = allelement.approve
      ca_model.updated = allelement.updated
      if(user.length == 0)
      {
        ca_model.isChecked = false
        this.category_array.push(ca_model)
      }else{
        var ischeck = false 
        user.forEach(userelement => {   
          if(allelement.ca_id == userelement.ca_id)
          {
            ca_model.isChecked = true
            ischeck = true
          }else{
            if(ischeck == false)
            {
              ca_model.isChecked = false
            }
          }

      });
      this.category_array.push(ca_model)
      }
        
    });
  }

  onchangestatus(index)
  {
    let model = this.category_array[index]
    model.isChecked = !model.isChecked
  }

  onclicksavebutton()
  {
    this.loadingctrl.showLoadingView()
    let userid = this.localstorage.getuserID()
   this.category_array.forEach(element => {
      if(element.isChecked == true)
      {
        this.restfulapi.deleteUserCategory(element.ca_id, userid).then(re=>{
          this.restfulapi.addUserCategory(element.ca_id, userid).then(res=>{
            this.toastctrl.create(res[''])
          })
        }).catch(er=>{

        })
       
      }
   });
   this.loadingctrl.removeLoadingView()   
  }


}
