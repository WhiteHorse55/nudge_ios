import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectcategorPage } from './selectcategor';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SelectcategorPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectcategorPage),
    ComponentsModule
  ],
})
export class SelectcategorPageModule {}
