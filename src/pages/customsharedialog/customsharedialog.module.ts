import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomsharedialogPage } from './customsharedialog';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CustomsharedialogPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomsharedialogPage),
    ComponentsModule
  ],
})
export class CustomsharedialogPageModule {}
