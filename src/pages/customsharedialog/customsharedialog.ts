import { SocialSharing } from '@ionic-native/social-sharing';
import { Component, Renderer } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  ActionSheetController
} from "ionic-angular";
import { ToastseriveProvider } from '../../providers/toastserive/toastserive';

/**
 * Generated class for the CustomsharedialogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var window : any;

@IonicPage()
@Component({
  selector: "page-customsharedialog",
  templateUrl: "customsharedialog.html"
})

export class CustomsharedialogPage {
  category : any
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public render: Renderer,
    public viewCtrl: ViewController,
    public actionSheetCtrl: ActionSheetController,
    public socialshare : SocialSharing,
    public toastctrl : ToastseriveProvider
  ) {
    this.category = {message : ""}
    this.category.message = this.navParams.get("message")
    console.log(this.category.message)
    this.render.setElementClass(
      viewCtrl.pageRef().nativeElement,
      "custom-share",
      true
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CustomsharedialogPage");
  }

  onclickdismissbutton() {
    this.viewCtrl.dismiss();
  }

  onclickRefreshbutton()
  {
    this.viewCtrl.dismiss();
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select You can change world by sharing",
      buttons: [
        {
          text: "Facebook",
          handler: () => {
            this.sharewithfacebook()
          }
        },

        {
          text: "Instgram",
          handler: () => {
            this.sharewithinstgram()
          }
        },

        {
          text: "Twitter",
          handler: () => {
            this.sharewithtwitter()
          }
        },

        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });

    actionSheet.present();
  }

  public sharewithtwitter() {
    console.log("this is share message", this.category.message)
    this.socialshare
      .shareViaTwitter(this.category.message,"","")
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install Twitter app first!");
      });
  }

  public sharewithinstgram() {
    this.socialshare
      .shareViaInstagram(this.category.message, "")
      .then(res => {})
      .catch(er => {
        this.toastctrl.create("Please install instagram app first!");
      });
  }

  public sharewithfacebook() {

    // this.socialshare.shareViaFacebookWithPasteMessageHint(this.category.message)
    this.socialshare
      .shareViaFacebook(this.category.message, this.category.message, this.category.message)
      .then(res => {
        console.log("this is result",res)
      })
      .catch(er => {
        this.toastctrl.create("Please install facebook app first!");
      });

  }
}
