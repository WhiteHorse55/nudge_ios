import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";
import { ResponseMessage } from "../../Constant/ResponseMessage";

/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-notification",
  templateUrl: "notification.html"
})
export class NotificationPage {
  notiArray: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public restfulapi : RestfulapiProvider,
    public localstorage : LocalstorageProvider,
    public toastctrl : ToastseriveProvider
  ) {
    this.notiArray = [];
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad NotificationPage");

  }

  ionViewWillEnter(){
    this.generateNotificationArray();
  }

  generateNotificationArray()
  {
    let userid = this.localstorage.getuserID()

    this.restfulapi.getNotificationList(userid).then(res=>{
      if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
      {
        this.notiArray = res['data']
      }else{
        // this.toastctrl.create(res['msg'])
      }

      }).catch(er=>{

    })
   }




  onclickitem(index) {
    let model = this.notiArray[index]
    console.log(model);

    this.restfulapi.sendNotificationSeen(model.noti_id).then(res=>{
      this.notiArray[index].is_seen = 1
      let modal = this.modalCtrl.create(
        "CustomnotificationdialogPage",
        {message: model.message},
        {
          showBackdrop: true,
          enableBackdropDismiss: true,
          cssClass: "inset-modal"
        }
      );
      modal.present();
    }).catch(er=>{
        this.toastctrl.create(er)
    })
  }

  onclickEditbutton() {}

  onclickdeletebutton(index) {
    let model = this.notiArray[index]
    this.restfulapi.deleteNotification(model.noti_id).then(res=>{
      this.notiArray.splice(index, 1);
    }).catch(er=>{
      this.toastctrl.create(er)
    })

  }
}
