import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {

  isRemoveOnly : any
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
    this.isRemoveOnly = false
    if(this.navParams.get("type") == 'remove')
    {
      this.isRemoveOnly = true
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }

  onclickdeletebutton()
  {
    this.viewCtrl.dismiss({type : "delete"})
  }

  onclickeditbutton()
  {
    this.viewCtrl.dismiss({type : "edit"})
  }

}
