import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationsettingPage } from './notificationsetting';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    NotificationsettingPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationsettingPage),
    ComponentsModule
  ],
})
export class NotificationsettingPageModule {}
