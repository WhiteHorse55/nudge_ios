import { onesignalAppId, sendor_id } from './../../app/config';
import { LocalstorageProvider } from './../../providers/localstorage/localstorage';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { NativeAudio } from "@ionic-native/native-audio";
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications';
import { elementClass } from '@angular/core/src/render3/instructions';
import { PushserviceProvider } from '../../providers/pushservice/pushservice';
import { OneSignal } from '@ionic-native/onesignal';
import { RestfulapiProvider } from '../../providers/restfulapi/restfulapi';
import { LoadingProvider } from '../../providers/loading/loading';
import { ToastseriveProvider } from '../../providers/toastserive/toastserive';

/**
 * Generated class for the NotificationsettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-notificationsetting",
  templateUrl: "notificationsetting.html"
})
export class NotificationsettingPage {
  isback: any;

  val_tone: any;
  val_sleephour: any;
  val_weight : any

  isvibrate: boolean;

  noti_array : Array<any>

  frequency_array : any
  weight_array : any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private nativeaudio: NativeAudio,
    private platform : Platform,
    private localstorage : LocalstorageProvider,
    private localnoti : LocalNotifications,
    private loadingctrl : LoadingProvider,
    private toastCtrl : ToastseriveProvider
  ) {
    this.isback = true;
    this.val_tone = "";
    this.val_weight = ""
    
    this.frequency_array = []
    this.weight_array = []

    this.val_sleephour = "";
    this.isvibrate = false

    this.noti_array = []

    this.generateArray()
    this.checkVibrationstatus()
  }


  ionViewWillEnter(){
   
  }

  ngAfterViewInit() {
    this.checkVibrationstatus()
  }

  checkVibrationstatus()
  {
    let noti_setting = this.localstorage.getNotificationInfo()
    console.log("notisetting", noti_setting);
    if(noti_setting)
    {
      this.checknotificationtime(noti_setting['time'])
      let vibration_status = noti_setting['vibration']
      let ringtone = noti_setting['tone']
      this.val_sleephour = noti_setting['sleep']
      this.val_tone = ringtone
      this.isvibrate = vibration_status
    }
  }

  checknotificationtime(timearray)
  {
    this.frequency_array = timearray
    // timearray.forEach(element => {
    //   console.log("this is val light",element)
       
    // });
  }

  onclickvibration() {
    this.isvibrate = !this.isvibrate;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad NotificationsettingPage");
  }

  generateArray(){
  
    this.weight_array = [
     
      {"name" : "Light","id" : 0},
      {"name" : "Medium","id" : 1},
      {"name" : "Heavy","id" : 2},
      {"name" : "None","id" : 4},
    ]
    this.frequency_array = [
      {"category" : "Monday", "daycode" : 1, "style" : 4},
      {"category" : "Tuesday", "daycode" : 2, "style" : 4},
      {"category" : "Wednesday", "daycode" : 3, "style" : 4},
      {"category" : "Thursday", "daycode" : 4, "style" : 4},
      {"category" : "Friday", "daycode" : 5, "style" : 4},
      {"category" : "Saturday", "daycode" : 6, "style" : 4},
      {"category" : "Sunday", "daycode" : 0, "style" : 4}
    ]
  }

  onclickBackbutton() {
    this.navCtrl.pop();
  }



  onchangeselectlight(val, index)
  {
    let model = this.frequency_array[index]
    model.style = Number(val) 
  }


  oncilckdonebutton()
  {
    this.loadingctrl.showLoadingView()
    var today = new Date().getTime();
    var delay = new Date(today);

    console.log("this is val",this.frequency_array);
  //  let res_array =  this.sortArray()

   let noti_info = {vibration : this.isvibrate, tone : this.val_tone, time : this.frequency_array, sleep : this.val_sleephour}
   this.localstorage.saveNotificationInfo(noti_info)

    if(this.val_tone == "")
    {
      this.loadingctrl.removeLoadingView()
      this.toastCtrl.create("Please choose message tone")
    }else{

      this.checkWeekday(this.frequency_array)
    }
  }

  checkWeekday(res_array)
  {
   console.log("this is res array :", res_array)
    res_array.forEach(element => {
      
      if(element.daycode == new Date().getDay())
      {
        console.log("this is daycode:", new Date().getDay())
        if(element.style == 2)
        {
           this.localnotification(8)
        }else if(element.style == 1)
        {
         this.localnotification(4)
        }else if(element.style == 0)
        {
         this.localnotification(1)
        }
      }
    });

    this.loadingctrl.removeLoadingView()
    this.navCtrl.pop()
  }

  localnotification(preiod)
  {
    if(this.isvibrate == true)
    {

      this.localnoti.schedule({
        text : "New Messages are received.",
        // trigger : {every: {hour : preiod} },
        trigger : {every: ELocalNotificationTriggerUnit.MINUTE},
        led : 'FF0000',
        sound : null,
        vibrate : true,
        data : {tone : this.val_tone, vib :this.isvibrate}
      })

    }else{
      this.localnoti.schedule({
        text : "New Messages are received.",
        // trigger : {every: {hour : preiod} },

        trigger : {every: ELocalNotificationTriggerUnit.MINUTE},
        led : 'FF0000',
        sound : null,
        vibrate : false,
        data : {tone : this.val_tone, vib :this.isvibrate}
      })
    }
    
    

  }



  sortArray()
  {
    var res_array = []
    this.noti_array.filter(item=>{
      var i = res_array.findIndex(x=>x.dayCode == item.dayCode && x.index < item.index);
      if(i <= -1)
      {
        res_array.push(item)
      }
      return null
    })

    return res_array
  }


  onchangetone(val)
  {
    if(this.val_tone == 'tone1')
    {
      this.playsound(this.val_tone)
    }else if(this.val_tone == 'tone2'){
      this.playsound(this.val_tone)
    }else{
      this.playsound(this.val_tone)
    }
  }



  playsound(soundname)
  {
    let unitid = ""
    if(this.platform.is('android'))
    {
      unitid = soundname + "_android"
    }else if(this.platform.is('ios')){
      unitid = soundname + "_ios"
    }else{

    }
    console.log(unitid)
    this.nativeaudio.play(unitid,()=>{
    })
  }
}
