import { Component, Renderer } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";

/**
 * Generated class for the GetstartedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-getstarted",
  templateUrl: "getstarted.html"
})
export class GetstartedPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewctrl: ViewController,
    public render: Renderer,
    public viewCtrl : ViewController
  ) {
    this.render.setElementClass(
      viewCtrl.pageRef().nativeElement,
      "custom-popup",
      true
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad GetstartedPage");
  }

  onclickdismissbutton() {
    this.viewctrl.dismiss();
  }

  onclickstarted() {
    this.viewctrl.dismiss();
  }
}
