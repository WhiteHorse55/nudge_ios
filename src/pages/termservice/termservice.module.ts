import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermservicePage } from './termservice';

@NgModule({
  declarations: [
    TermservicePage,
  ],
  imports: [
    IonicPageModule.forChild(TermservicePage),
    ComponentsModule
  ],
})
export class TermservicePageModule {}
