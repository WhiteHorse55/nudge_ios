import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResponseMessage } from '../../Constant/ResponseMessage';

/**
 * Generated class for the TermservicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-termservice',
  templateUrl: 'termservice.html',
})
export class TermservicePage {

  isback : any
  content : any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.isback = true

  }

  ionViewDidLoad() {
    this.content = ResponseMessage.TERMSERVICE
    console.log('ionViewDidLoad TermservicePage');
  }

  onclickBackbutton()
  {
    this.navCtrl.pop()
  }

  onclickagree()
  {
    this.navCtrl.setRoot('SigninPage')
  }

  onclickdisagree()
  {
    this.navCtrl.setRoot('SigninPage')
  }

}
