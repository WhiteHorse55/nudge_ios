import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddmessagePage } from './addmessage';

@NgModule({
  declarations: [
    AddmessagePage,
  ],
  imports: [
    IonicPageModule.forChild(AddmessagePage),
    ComponentsModule
  ],
})
export class AddmessagePageModule {}
