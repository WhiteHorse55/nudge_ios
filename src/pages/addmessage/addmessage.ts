import { ResponseMessage } from './../../Constant/ResponseMessage';
import { ViewController } from "ionic-angular";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { CustomalertProvider } from '../../providers/customalert/customalert';

/**
 * Generated class for the AddmessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-addmessage",
  templateUrl: "addmessage.html"
})
export class AddmessagePage {
  isback: any;
  isPersonal: any;
  isShow : any

  message: any;
  info : any;
  category_id : any
  edit_type : any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toastservice : ToastseriveProvider,
    public restfulapi : RestfulapiProvider,
    public localprovider : LocalstorageProvider,
    public alertprovider : CustomalertProvider
  ) {
    this.isback = true;
    this.isPersonal = true;
    this.isShow = false
    this.message = "";
    this.edit_type = "";

    this.edit_type = this.navParams.get("type");
    this.category_id = this.navParams.get('ca_id')

    if (this.edit_type == "edit") {
      this.info = this.navParams.get("info");
      this.message = this.navParams.get("info")["message"];

      if(this.navParams.get("info")['active'] == "true")
      {
        this.isPersonal = true
      }else{
        this.isPersonal = false
      }
    }else if(this.edit_type == 'show'){
      this.info = this.navParams.get("info");
      this.isShow = true
      this.message = this.navParams.get("info")["message"];

      if(this.navParams.get("info")['active'] == 'true')
      {
        this.isPersonal = true
      }else{
        this.isPersonal = false
      }

    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AddmessagePage");
  }

  onclickpseronalcheckbox() {
    this.isPersonal = false;
  }

  onclickpubliccheckbox() {
    this.isPersonal = true;
  }

  onclickclose() {
    this.message = "";
    this.viewCtrl.dismiss(this.message);
  }

  onclicksavebutton() {
    if (this.message == "") {
      this.toastservice.create("Message is empty");
    } else {
      if(this.edit_type == 'edit')
      {
        this.editMessage()
      }else{
        this.addMessage()
      }
      // this.viewCtrl.dismiss(this.message);
    }
  }

  addMessage()
  {
    this.restfulapi.addmessage(this.category_id, this.isPersonal,this.message, this.getUserid()).then(res=>{
      if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
      {
        this.viewCtrl.dismiss()
      }else{
        this.toastservice.create(res['msg'])
      }
      this.viewCtrl.dismiss();
    }).catch(er=>{
      this.alertprovider.presentErrorAlert(er)
    })
  }

  editMessage()
  {
    this.restfulapi.editmessage(this.category_id, this.isPersonal,this.message, this.getUserid(), this.info['id']).then(res=>{
      if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
      {
        this.viewCtrl.dismiss()
      }else{
        this.toastservice.create(res['msg'])
      }
      this.viewCtrl.dismiss();
    }).catch(er=>{
      this.alertprovider.presentErrorAlert(er)
    })
  }

  getUserid()
  {
    return this.localprovider.getuserID()
  }
}
