import { ResponseMessage } from './../../Constant/ResponseMessage';
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  ModalController
} from "ionic-angular";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { ToastseriveProvider } from '../../providers/toastserive/toastserive';
import { CustomalertProvider } from '../../providers/customalert/customalert';

/**
 * Generated class for the FavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-favorite",
  templateUrl: "favorite.html"
})
export class FavoritePage {
  favoriteArray: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl : ModalController,
    public restfulapi : RestfulapiProvider,
    public localprovider : LocalstorageProvider,
    public toastctrl : ToastseriveProvider,
    public alertCtrl : CustomalertProvider
  ) {
    this.favoriteArray = [];
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad FavoritePage");

  }

  ionViewWillEnter(){
    this.getFavoriteMessage()
  }

  public getFavoriteMessage()
  {
    let myid = this.getuserID()
    this.restfulapi.getFavorite(myid).then(res=>{
        this.favoriteArray = res['data']
    }).catch(er=>{

    })
  }


  onclickEditOptions(index) {
    let myid = this.getuserID()
    let fa_id = this.favoriteArray[index]['fa_id']
    this.alertCtrl.presentAlertWithCallback("Are you sure delete this message","").then(res=>{
      this.restfulapi.deleteFavorite(fa_id, myid).then(res=>{

        if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
        {
          this.favoriteArray.splice(index, 1);
        }else{
          // this.toastctrl.create(res['msg'])
        }

      }).catch(er=>{
        // this.toastctrl.create(er)
      })
    }).catch(er=>{
      // this.toastctrl.create(er)
    })
  }

  onclickItem(index)
  {
    let model = this.favoriteArray[index]
    let modal = this.modalCtrl.create('CustomsharedialogPage',{message : model.message} ,{showBackdrop:true, enableBackdropDismiss:true, cssClass : 'inset-modal'});
    modal.present();
  }

  getuserID()
  {
    return this.localprovider.getuserID()
  }
}
