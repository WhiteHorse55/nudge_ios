import { Component, OnInit } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { CustomalertProvider } from "../../providers/customalert/customalert";
import { LoadingProvider } from "../../providers/loading/loading";
import { ResponseMessage } from "../../Constant/ResponseMessage";

/**
 * Generated class for the SignupFirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-signup-first",
  templateUrl: "signup-first.html"
})
export class SignupFirstPage implements OnInit {

  fromPage = "";
  isback: any;

  validations_form: any;
  useremail : any
  is_notuser : any
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public restfulprovider: RestfulapiProvider,
    public customalertctrl : CustomalertProvider,
    public loadingctrl : LoadingProvider
  ) {
    this.fromPage = this.navParams.get("from");
    this.isback = true;
    this.useremail = ""
    this.is_notuser = true
  }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
        ])
      )
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SignupFirstPage");
  }

  onclicknextbutton() {
    this.checkEmailExist()
  }

  onclickBackbutton() {
    this.navCtrl.pop();
  }

  gotoLoginPage() {
    this.navCtrl.setRoot("SigninPage");
  }

  checkEmailExist()
  {
    this.loadingctrl.showLoadingView()
    this.restfulprovider.checkEmailExist(this.useremail).then(res=>{
      this.loadingctrl.removeLoadingView()
      console.log(res)
      if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
      {
        this.navCtrl.push("SignupSecondPage",{email : this.useremail});
      }else{
        this.is_notuser = false
        this.customalertctrl.presentErrorAlert(ResponseMessage.EMAIL_EXIST)
      }

    }).catch(er=>{
      this.loadingctrl.removeLoadingView()
      this.customalertctrl.presentErrorAlert(er)
    })
  }
}
