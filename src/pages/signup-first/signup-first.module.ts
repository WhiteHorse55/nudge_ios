import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupFirstPage } from './signup-first';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SignupFirstPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupFirstPage),
    ComponentsModule
  ],
})
export class SignupFirstPageModule {}
