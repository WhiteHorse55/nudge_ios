import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { onesignalAppId, sendor_id } from './../../app/config';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { ValidationProvider } from "../../providers/validation/validation";
import { LoadingProvider } from "../../providers/loading/loading";
import { CustomalertProvider } from "../../providers/customalert/customalert";
import { ResponseMessage } from "../../Constant/ResponseMessage";
import { ToastseriveProvider } from "../../providers/toastserive/toastserive";
import { RestfulapiProvider } from "../../providers/restfulapi/restfulapi";
import { OneSignal } from "@ionic-native/onesignal";
import { GooglePlus } from '@ionic-native/google-plus';


/**
 * Generated class for the SigninPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-signin",
  templateUrl: "signin.html"
})
export class SigninPage {
  userinfo : any
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public localstoragerprovider: LocalstorageProvider,
    public validationprovider : ValidationProvider,
    public loadingprovider : LoadingProvider,
    public customralertprovider : CustomalertProvider,
    public toastprovider : ToastseriveProvider,
    public restfulprovider : RestfulapiProvider,
    public onesignal : OneSignal,
    public fb : Facebook,
    public googlelogin : GooglePlus,
    public toastservice : ToastseriveProvider,
    public platform : Platform
  ) {
    this.userinfo = {
      email : "",
      password : ""
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SigninPage");
    this.saveIsFirstSatatus()
  }


  // email login
  onclickLoginButton() {
    this.loadingprovider.showLoadingView()
    this.validationprovider.signinValidation(this.userinfo).then(res=>{

      this.restfulprovider.signin(this.userinfo.email, this.userinfo.password).then(result=>{
        this.loadingprovider.removeLoadingView()

        if(result['status'] == ResponseMessage.RESPONSE_SUCCESS)
        {
          this.localstoragerprovider.saveUserInfo(result['data'])

          if(this.platform.is('ios') && this.platform.is('android'))
          {
            this.onesignal.startInit(onesignalAppId, sendor_id);
            this.onesignal.getIds().then(res=>{
               this.restfulprovider.saveUserToken(result['data']['id'], res['userId']).then(res=>{
                 this.navCtrl.setRoot("CustomtabPage");
               }).catch(er=>{
                 console.log("erpr", er);
               })
             }).catch(error=>{
               console.log("erpr", error);
             })
             this.onesignal.endInit()

          }else{
            this.navCtrl.setRoot("CustomtabPage");
          }

        }else{
          this.loadingprovider.removeLoadingView()
          this.toastprovider.create(result['msg'], false, 2000)
        }

      }).catch(er=>{
        this.toastprovider.create(er, false, 2000)
        this.loadingprovider.removeLoadingView()
      })


    }).catch(er=>{
      this.loadingprovider.removeLoadingView()
      this.customralertprovider.presentErrorAlert(er)
    })

  }

  onclickforgotpass() {
    this.navCtrl.push("ForgotpassPage");
  }

  onclicksignupbutton() {
    this.navCtrl.push("SignupFirstPage", { from: "signin" });
  }

  saveIsFirstSatatus() {
    this.localstoragerprovider.saveFirstStatus(true);
  }

  onclickFacebookButton()
  {

    const permissions = ["public_profile", "email", 'user_photos']
    this.fb.login(permissions).then((res : FacebookLoginResponse)=>{
        if(res.status == "connected")
        {
          var fa_id = res.authResponse.userID
          var fa_token = res.authResponse.accessToken
          this.fb.api("/me?fields=name,email", permissions).then(user=>{
              user.picture =  "https://graph.facebook.com/" + fa_id + "/picture?type=large";
                var name      = user.name;
                var email     = user.email;

                this.restfulprovider.checkEmailExist(email).then(res=>{
                    console.log("email exist ", res);

                    this.loadingprovider.showLoadingView()
                    if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
                    {
                      this.signUpWithFB(email, fa_id, this.getFLName(name), user.picture)
                    }else{
                      this.signinWithFB(email, fa_id)
                    }

                }).catch(er=>{
                  console.log("email exist ", er);
                })

          })
        }
    })
  }

  getFLName(str_name : string)
  {
      return str_name.split(" ")
  }


  signUpWithFB(email, password, usernames, photo="")
  {

    let info = {email : email,
      password : password,
      firstname : usernames[0],
      lastname :  usernames[1],
      photo : photo}
    this.restfulprovider.signup(info).then(re=>{
      if(re['status'] = ResponseMessage.RESPONSE_SUCCESS)
      {
        this.signinWithFB(email, password)
      }else{
        this.loadingprovider.showLoadingView()
        this.toastservice.create(re['msg'])
      }

    }).catch(err=>{
      this.loadingprovider.removeLoadingView()
    })
  }


  signUpWithGoogle(email, password, usrname, photo="")
  {

    let info = {email : email,
      password : password,
      firstname : usrname,
      lastname :  usrname,
      photo : photo}
    this.restfulprovider.signup(info).then(re=>{
      if(re['status'] = ResponseMessage.RESPONSE_SUCCESS)
      {
        this.signinWithFB(email, password)
      }else{
        this.loadingprovider.showLoadingView()
        this.toastservice.create(re['msg'])
      }

    }).catch(err=>{
      this.loadingprovider.removeLoadingView()
    })
  }

  signinWithFB(email, password)
  {
    this.restfulprovider.signin(email, password).then(result=>{
      this.loadingprovider.removeLoadingView()
      console.log("step signinwithfb", result);

      if(result['status'] == ResponseMessage.RESPONSE_SUCCESS)
      {
        this.localstoragerprovider.saveUserInfo(result['data'])
        if(this.platform.is('ios') && this.platform.is('android'))
          {
            this.onesignal.startInit(onesignalAppId, sendor_id);
            this.onesignal.getIds().then(res=>{
               this.restfulprovider.saveUserToken(result['data']['id'], res['userId']).then(res=>{
                 this.navCtrl.setRoot("CustomtabPage");
               }).catch(er=>{
                 console.log("erpr", er);
               })
             }).catch(error=>{
               console.log("erpr", error);
             })
             this.onesignal.endInit()

          }else{
            this.navCtrl.setRoot("CustomtabPage");
          }

      }else{
        this.toastprovider.create(result['msg'], false, 2000)
      }

    }).catch(er=>{
      this.toastprovider.create(er, false, 2000)
      this.loadingprovider.removeLoadingView()
    })
  }

  onclickGoogleButton()
  {
    this.googlelogin.login({}).then(res=>{
        console.log("this is google login", res);
        let email = res['email']
        let username = res['displayName']
        let userid = res['userId']

        this.restfulprovider.checkEmailExist(email).then(res=>{
          console.log("email exist ", res);

          this.loadingprovider.showLoadingView()
          if(res['status'] == ResponseMessage.RESPONSE_SUCCESS)
          {
            this.signUpWithGoogle(email, userid, username, "")
          }else{
            this.signinWithFB(email, userid)
          }

      }).catch(er=>{
        console.log("email exist ", er);
      })


    }).catch(er=>{
      console.log("this is error", er)
    })
  }

  onclickInstagramButton()
  {

  }

}
