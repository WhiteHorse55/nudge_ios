import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomtabPage } from './customtab';

@NgModule({
  declarations: [
    CustomtabPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomtabPage),
  ],
})
export class CustomtabPageModule {}
