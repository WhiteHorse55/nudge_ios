import { onesignalAppId, sendor_id } from './../../app/config';
import { OneSignal } from '@ionic-native/onesignal';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { LocalstorageProvider } from "../../providers/localstorage/localstorage";
import { RestfulapiProvider } from '../../providers/restfulapi/restfulapi';

/**
 * Generated class for the CustomtabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-customtab",
  templateUrl: "customtab.html"
})
export class CustomtabPage {
  tab1Root = "MainPage";
  tab2Root = "NotificationPage";
  tab3Root = "FavoritePage";
  tab4Root = "SettingPage";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public localstorage: LocalstorageProvider,
    public onesignal : OneSignal,
    public restfulapi : RestfulapiProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad CustomtabPage");
    this.getuserToken()
  }

  public async getuserToken()
  {

    return new Promise((resolve, reject)=>{
      let userid = this.localstorage.getuserID()
      this.onesignal.startInit(onesignalAppId, sendor_id);
      this.onesignal.inFocusDisplaying(this.onesignal.OSInFocusDisplayOption.InAppAlert);
      this.onesignal.getIds().then(result=>{
        console.log("this is token info", result);
        this.restfulapi.saveUserToken(userid, result['userId']).then(async res=>{
            resolve( result['userId'])
        }).catch(er=>{
          reject(er)
        })

      }).catch(er=>{
        reject(er)
      })
      this.onesignal.endInit()
    })
  }
}
